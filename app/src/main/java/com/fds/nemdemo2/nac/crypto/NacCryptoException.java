package com.fds.nemdemo2.nac.crypto;

import com.fds.nemdemo2.nac.exceptions.NacException;

public class NacCryptoException extends NacException {
    public NacCryptoException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }
}
