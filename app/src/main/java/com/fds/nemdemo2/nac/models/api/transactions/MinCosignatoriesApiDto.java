package com.fds.nemdemo2.nac.models.api.transactions;

public final class MinCosignatoriesApiDto {
	/**
	 * Value indicating the relative change of the minimum cosignatories.
	 */
	public int relativeChange;
}
