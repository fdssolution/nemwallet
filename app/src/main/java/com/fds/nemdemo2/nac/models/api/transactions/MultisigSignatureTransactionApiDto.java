package com.fds.nemdemo2.nac.models.api.transactions;

import com.fds.nemdemo2.nac.common.models.HashValue;
import com.fds.nemdemo2.nac.models.BinaryData;
import com.fds.nemdemo2.nac.models.primitives.AddressValue;

public class MultisigSignatureTransactionApiDto extends AbstractTransactionApiDto {
    /**
     * The transaction signature.
     */
    public BinaryData signature;

    /**
     * The hash of the inner transaction of the corresponding multisig transaction.
     */
    public HashValue otherHash;
    /**
     * The address of the corresponding multisig account.
     */
    public AddressValue otherAccount;
}
