package com.fds.nemdemo2.nac.models.api.transactions;

import android.support.annotation.Nullable;

import com.fds.nemdemo2.nac.enums.ImportanceTransferMode;
import com.fds.nemdemo2.nac.models.BinaryData;
import com.fds.nemdemo2.nac.models.NacPublicKey;

public class ImportanceTransferTransactionApiDto extends AbstractTransactionApiDto {
    /**
     * The transaction signature (missing if part of a multisig transaction).
     */
    @Nullable
    public BinaryData signature;
    /**
     * The mode.
     */
    public ImportanceTransferMode mode;
    /**
     * The public key of the receiving account as hexadecimal string.
     */
    public NacPublicKey remoteAccount;
}
