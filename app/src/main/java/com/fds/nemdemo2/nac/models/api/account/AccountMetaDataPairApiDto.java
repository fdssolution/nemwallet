package com.fds.nemdemo2.nac.models.api.account;

public final class AccountMetaDataPairApiDto {
	public AccountInfoApiDto account;
	public AccountMetaDataApiDto meta;
}
