package com.fds.nemdemo2.nac.exceptions;

public final class AddressFormatRuntimeException extends NacRuntimeException {
	public AddressFormatRuntimeException(String detailMessage) {
		super(detailMessage);
	}
}
