package com.fds.nemdemo2.nac.models.api.transactions;

public final class TransactionMetaDataPairApiDto {
	public TransactionMetaDataApiDto meta;
	public AbstractTransactionApiDto transaction;
}
