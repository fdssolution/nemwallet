package com.fds.nemdemo2.nac.models.api.account;

import android.support.annotation.Nullable;

import com.fds.nemdemo2.R;
import com.fds.nemdemo2.application.AppController;
import com.fds.nemdemo2.nac.models.NacPublicKey;
import com.fds.nemdemo2.nac.models.Xems;
import com.fds.nemdemo2.nac.models.primitives.AddressValue;
import com.fds.nemdemo2.restapi.models.MultisigInfo;

public final class AccountInfoApiDto {
	public AddressValue address;
	public NacPublicKey publicKey;
	/**
	 * Always null according to specs.
	 */
	@Nullable
	public String label;
	public Xems balance;
	public Xems vestedBalance;
	public double       importance;
	public long         harvestedBlocks;
	@Nullable
	public MultisigInfo multisigInfo;

	@Override
	public String toString() {
		if(publicKey == null) {
			return AppController.getResString(R.string.errormessage_no_public_key);
		}
		return publicKey.toString();
	}
}
