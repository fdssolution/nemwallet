package com.fds.nemdemo2.nac.exceptions;

import android.support.annotation.StringRes;

import com.fds.nemdemo2.application.AppController;

/**
 * Exception which message can be shown to user.
 */
public final class NacUserVisibleException extends NacException {
    public NacUserVisibleException(@StringRes final int detailMessageRes) {
        super(AppController.getAppContext().getString(detailMessageRes));
    }

    public NacUserVisibleException(@StringRes final int detailMessageRes, final Throwable throwable) {
        super(AppController.getAppContext().getString(detailMessageRes), throwable);
    }
}
