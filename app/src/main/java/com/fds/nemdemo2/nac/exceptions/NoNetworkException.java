package com.fds.nemdemo2.nac.exceptions;

/**
 * Thrown when app hosting device has no network connection.
 */
public final class NoNetworkException extends NacException {
}
