package com.fds.nemdemo2.nac.models;

import android.support.annotation.NonNull;

import com.fds.nemdemo2.core.crypto.PrivateKey;
import com.fds.nemdemo2.nac.crypto.NacCryptoException;

public final class NacPrivateKey extends BinaryData {

    public NacPrivateKey(@NonNull final byte[] raw) {
        super(raw);
    }

    public NacPrivateKey(@NonNull final String rawDataHex) {
        super(rawDataHex);
    }

    @NonNull
    public EncryptedNacPrivateKey encryptKey(@NonNull final BinaryData encryptionKey)
            throws NacCryptoException {
        final EncryptedBinaryData encryptedData = super.encrypt(encryptionKey);
        return new EncryptedNacPrivateKey(encryptedData.rawData);
    }

    @NonNull
    public PrivateKey toPrivateKey() {
        return PrivateKey.fromBytes(this.rawData);
    }
}
