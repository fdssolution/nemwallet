package com.fds.nemdemo2.nac.models.api.transactions;

import com.fds.nemdemo2.nac.models.BinaryData;

public final class UnconfirmedTransactionMetaDataApiDto {
    /**
     * The hash of the inner transaction or null if the transaction is not a multisig transaction.
     */
    public BinaryData data;
}
