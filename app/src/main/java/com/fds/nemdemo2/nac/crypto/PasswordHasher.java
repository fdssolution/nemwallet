package com.fds.nemdemo2.nac.crypto;

import com.fds.nemdemo2.application.AppConstants;

import org.mindrot.jbcrypt.BCrypt;

public final class PasswordHasher {
    public static String hash(final String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt(AppConstants.BCRYPT_LOG_ROUNDS));
    }

    public static boolean check(final String password, final String hash) {
        return BCrypt.checkpw(password, hash);
    }
}
