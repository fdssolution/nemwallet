package com.fds.nemdemo2.utls;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings.Secure;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

//import org.apache.http.conn.utilyes.InetAddressUtils;

public class Utilities {

    public static boolean isNetworkAvailable(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        boolean haveEthernet = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getType() == ConnectivityManager.TYPE_WIFI)
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getType() == ConnectivityManager.TYPE_MOBILE)
                if (ni.isConnected())
                    haveConnectedMobile = true;
            if (ni.getType() == ConnectivityManager.TYPE_ETHERNET)
                if (ni.isConnected())
                    haveEthernet = true;
        }
        return haveConnectedWifi || haveConnectedMobile || haveEthernet;
    }

    public static String getAndroidID(Context context) {
        String androidID = Secure.getString(context.getContentResolver(),
                Secure.ANDROID_ID);

        return androidID;
    }

    /*public static String getIMEI(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String IMEI = telephonyManager.getDeviceId();

        return IMEI;
    }*/

	/*public static String getMacAddress(Context context){
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		WifiInfo wInfo = wifiManager.getConnectionInfo();
		
		String mac = wInfo.getMacAddress();
		
		return mac;
	}*/

    /**
     * Returns MAC address of the given interface name.
     *
     * @param interfaceName eth0, wlan0 or NULL=use first interface
     * @return mac address or empty string
     */
    public static String getMACAddress(String interfaceName) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac == null) return "";
                StringBuilder buf = new StringBuilder();
                for (int idx = 0; idx < mac.length; idx++)
                    buf.append(String.format("%02X:", mac[idx]));
                if (buf.length() > 0) buf.deleteCharAt(buf.length() - 1);
                return buf.toString();
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
        /*try {
            // this is so Linux hack
            return loadFileAsString("/sys/class/net/" +interfaceName + "/address").toUpperCase().trim();
        } catch (IOException ex) {
            return null;
        }*/
    }

    public static String getWifiOrEth0MACAddress() {
        String macAddress;

        macAddress = getMACAddress("wlan0");

        if (macAddress.equals("")) {
            macAddress = getMACAddress("eth0");
        }

        return macAddress;
    }

    /**
     * Get IP address from first non-localhost interface
     *
     * @param ipv4 true=return ipv4, false=return ipv6
     * @return address or empty string
     */
    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':') < 0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim < 0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }

    // Get device name
    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    // get version of this app
    public static String getCurrentVersionApp(Context context) {
        PackageManager manager = context.getPackageManager();
        String currentVersion = "";

        try {
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
//			   String packageName = info.packageName;
            int versionCode = info.versionCode;
            String versionName = info.versionName;

//            currentVersion = versionName + "-" + versionCode;

            currentVersion = versionName;
        } catch (NameNotFoundException e) {
            return "";
        }

        return currentVersion;
    }

    // get version of this app
    public static String getCurrentVersionCode(Context context) {
        PackageManager manager = context.getPackageManager();
        String currentVersion = "";

        try {
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            int versionCode = info.versionCode;

            currentVersion = versionCode + "";
        } catch (NameNotFoundException e) {
            return "";
        }

        return currentVersion;
    }

    public static String getOSVersion() {
        return Build.VERSION.SDK_INT + "-" + "Android " + Build.VERSION.RELEASE;
    }

    public static String getScreenSize(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);

        wm.getDefaultDisplay().getMetrics(metrics);

        return metrics.widthPixels + "x" + metrics.heightPixels;
    }

  /*  public static boolean isTablet(Context context) {
        return context.getResources().getBoolean(R.bool.isTablet);
    }*/

    public static String durationInMilliSecondsToString(int msec) {
        int totalSeconds = msec / 1000;
        int hours = totalSeconds / 3600;
        int minutes = (totalSeconds / 60) - (hours * 60);
        int seconds = totalSeconds - (hours * 3600) - (minutes * 60);
        String formatted = String.format("%d:%02d:%02d", hours, minutes, seconds);

        return formatted;
    }

    public static boolean checkIsCellPhone(String phoneNumber) {
        try {
            if (phoneNumber.equals("")) {
                return false;
            }

            if (phoneNumber.length() < 10 || phoneNumber.length() > 11) {
                return false;
            }

            Integer.parseInt(phoneNumber);

        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public static boolean checkEmailValid(CharSequence email) {
        return !email.equals("") && android.util.Patterns.EMAIL_ADDRESS.matcher(email)
                .matches();
    } // end of TextWatcher (email)

    public static boolean checkIsNumberKey(int keycode) {
        return keycode == KeyEvent.KEYCODE_0 || keycode == KeyEvent.KEYCODE_1
                || keycode == KeyEvent.KEYCODE_1
                || keycode == KeyEvent.KEYCODE_2
                || keycode == KeyEvent.KEYCODE_3
                || keycode == KeyEvent.KEYCODE_4
                || keycode == KeyEvent.KEYCODE_5
                || keycode == KeyEvent.KEYCODE_6
                || keycode == KeyEvent.KEYCODE_7
                || keycode == KeyEvent.KEYCODE_8
                || keycode == KeyEvent.KEYCODE_9
                || keycode == KeyEvent.KEYCODE_NUMPAD_0
                || keycode == KeyEvent.KEYCODE_NUMPAD_1
                || keycode == KeyEvent.KEYCODE_NUMPAD_2
                || keycode == KeyEvent.KEYCODE_NUMPAD_3
                || keycode == KeyEvent.KEYCODE_NUMPAD_4
                || keycode == KeyEvent.KEYCODE_NUMPAD_5
                || keycode == KeyEvent.KEYCODE_NUMPAD_6
                || keycode == KeyEvent.KEYCODE_NUMPAD_7
                || keycode == KeyEvent.KEYCODE_NUMPAD_8
                || keycode == KeyEvent.KEYCODE_NUMPAD_9;

    }

    public static String getNumberKey(int keycode) {
        String key = "";

        if (keycode == KeyEvent.KEYCODE_0
                || keycode == KeyEvent.KEYCODE_NUMPAD_0) {
            key = "0";
        } else if (keycode == KeyEvent.KEYCODE_1
                || keycode == KeyEvent.KEYCODE_NUMPAD_1) {
            key = "1";
        } else if (keycode == KeyEvent.KEYCODE_2
                || keycode == KeyEvent.KEYCODE_NUMPAD_2) {
            key = "2";
        } else if (keycode == KeyEvent.KEYCODE_3
                || keycode == KeyEvent.KEYCODE_NUMPAD_3) {
            key = "3";
        } else if (keycode == KeyEvent.KEYCODE_4
                || keycode == KeyEvent.KEYCODE_NUMPAD_4) {
            key = "4";
        } else if (keycode == KeyEvent.KEYCODE_5
                || keycode == KeyEvent.KEYCODE_NUMPAD_5) {
            key = "5";
        } else if (keycode == KeyEvent.KEYCODE_6
                || keycode == KeyEvent.KEYCODE_NUMPAD_6) {
            key = "6";
        } else if (keycode == KeyEvent.KEYCODE_7
                || keycode == KeyEvent.KEYCODE_NUMPAD_7) {
            key = "7";
        } else if (keycode == KeyEvent.KEYCODE_8
                || keycode == KeyEvent.KEYCODE_NUMPAD_8) {
            key = "8";
        } else if (keycode == KeyEvent.KEYCODE_9
                || keycode == KeyEvent.KEYCODE_NUMPAD_9) {
            key = "9";
        }

        return key;
    }

    public static String EncryptString(String s) {
        String encrypted = "";
        MCrypt mcrypt = new MCrypt();

        try {
            encrypted = MCrypt.bytesToHex(mcrypt.encrypt(s));

        } catch (Exception e) {

            e.printStackTrace();
        }

        return encrypted;
    }

    public static String EncryptString(String secrectKey, String s) {
        String encrypted = "";
        MCrypt mcrypt = new MCrypt(secrectKey);

        try {
            encrypted = MCrypt.bytesToHex(mcrypt.encrypt(s));
        } catch (Exception e) {

            e.printStackTrace();
        }

        return encrypted;
    }

    public static String EncryptString(String secrectKey, String iv, String s) {
        String encrypted = "";
        MCrypt mcrypt = new MCrypt(secrectKey, iv);

        try {
            encrypted = MCrypt.bytesToHex(mcrypt.encrypt(s));
        } catch (Exception e) {

            e.printStackTrace();
        }

        return encrypted;
    }

    public static String DecryptString(String s) {
        String decrypted = "";
        MCrypt mcrypt = new MCrypt();

        try {
            decrypted = new String(mcrypt.decrypt(s));
            decrypted = decrypted.trim();
        } catch (Exception e) {

            e.printStackTrace();
        }

        return decrypted;
    }

    public static String DecryptString(String secrectKey, String s) {
        String decrypted = "";
        MCrypt mcrypt = new MCrypt(secrectKey);

        try {
            decrypted = new String(mcrypt.decrypt(s));
            decrypted = decrypted.trim();
        } catch (Exception e) {

            e.printStackTrace();
        }

        return decrypted;
    }

    public static String DecryptString(String secrectKey, String iv, String s) {
        String decrypted = "";
        MCrypt mcrypt = new MCrypt(secrectKey, iv);

        try {
            decrypted = new String(mcrypt.decrypt(s));
            decrypted = decrypted.trim();
        } catch (Exception e) {

            e.printStackTrace();
        }

        return decrypted;
    }

    // String.Join in Java 8
    public static <T> String stringJoin(T[] array, String cement) {
        StringBuilder builder = new StringBuilder();

        if (array == null || array.length == 0) {
            return null;
        }

        for (T t : array) {
            builder.append(t).append(cement);
        }

        //Xoa ky tu ngan cach "cement" o cuoi dong
        builder.delete(builder.length() - cement.length(), builder.length());

        return builder.toString();
    }


    /**
     * Hàm MD5
     *
     * @param s String muốn md5
     * @return String đã md5
     */
    public static String md5Hash(String s) {
        StringBuffer MD5Hash = new StringBuffer();

        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                MD5Hash.append(h);
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return MD5Hash.toString();
    }

    public static String base64(String s) {
        byte[] data = s.getBytes();
        return Base64.encodeToString(data, Base64.DEFAULT);
    }

    private static String convertToHex(byte[] data) {
        StringBuilder buf = new StringBuilder();
        for (byte b : data) {
            int halfbyte = (b >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                buf.append((0 <= halfbyte) && (halfbyte <= 9) ? (char) ('0' + halfbyte) : (char) ('a' + (halfbyte - 10)));
                halfbyte = b & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String SHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        byte[] textBytes = text.getBytes("iso-8859-1");
        md.update(textBytes, 0, textBytes.length);
        byte[] sha1hash = md.digest();
        return convertToHex(sha1hash);
    }

	/*public static void showVirtualKeyboard(Context context ){
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
	}
	
	public static void hideVirtualKeyboard(Context context ){
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
	}*/

    public static String replaceAnyTypeIssueWebService(String s) {
        return s.replace("anyType{}", "");
    }

    public static void recycleImagesFromView(View view) {
        if (view instanceof ImageView) {
            Drawable drawable = ((ImageView) view).getDrawable();
            if (drawable instanceof BitmapDrawable) {
                BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
                bitmapDrawable.getBitmap().recycle();
            }
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                recycleImagesFromView(((ViewGroup) view).getChildAt(i));
            }
        }
    }


    public static void showVirtualKeyboard(Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public static void hideVirtualKeyboard(Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    //Ham tao PsuedoUniqueID de dinh danh tung device Android
    public static String getPsuedoUniqueID() {
        // Try not to use DISPLAY, HOST or ID - these items could change.
        // If there are collisions, there will be overlapping data

        int cpuABILenght = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cpuABILenght = Build.SUPPORTED_ABIS[0].length();
        } else {
            cpuABILenght = Build.CPU_ABI.length();
        }

        /*Log.e("Inizio", "getPsuedoUniqueID: BOARD " + Build.BOARD);
        Log.e("Inizio", "getPsuedoUniqueID: BRAND " + Build.BRAND);
        Log.e("Inizio", "getPsuedoUniqueID: DEVICE " + Build.DEVICE);
        Log.e("Inizio", "getPsuedoUniqueID: MANUFACTURER " + Build.MANUFACTURER);
        Log.e("Inizio", "getPsuedoUniqueID: MODEL " + Build.MODEL);
        Log.e("Inizio", "getPsuedoUniqueID: PRODUCT " + Build.PRODUCT);*/

        String m_szDevIDShort = "35" +
                (Build.BOARD.length() % 10)
                + (Build.BRAND.length() % 10)
                + (cpuABILenght % 10)
                + (Build.DEVICE.length() % 10)
                + (Build.MANUFACTURER.length() % 10)
                + (Build.MODEL.length() % 10)
                + (Build.PRODUCT.length() % 10);

        // Only devices with API >= 9 have android.os.Build.SERIAL
        // http://developer.android.com/reference/android/os/Build.html#SERIAL
        // If a user upgrades software or roots their phone, there will be a duplicate entry
        String serial = null;
        try {
            serial = android.os.Build.class.getField("SERIAL").get(null).toString();

//            Log.e("Inizio", "getPsuedoUniqueID: serial" + serial);

            // Go ahead and return the serial for api => 9
            return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
        } catch (Exception e) {
            // String needs to be initialized
            serial = "serialSCTV"; // some value
        }

        // Finally, combine the values we have found by using the UUID class to create a unique identifier
        return new UUID(m_szDevIDShort.hashCode(), serial.hashCode()).toString();
    }

    /*public static String getMixUID(Context context) {
        //Lay android ID
        String m_szAndroidID = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);

        //Lay IMEI
        TelephonyManager tm = (TelephonyManager) context
                .getSystemService(context.TELEPHONY_SERVICE);
        String m_szImei = tm.getDeviceId();


        //Lay getPsuedoUniqueID
        int cpuABILenght = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cpuABILenght = Build.SUPPORTED_ABIS[0].length();
        } else {
            cpuABILenght = Build.CPU_ABI.length();
        }

        String m_szDevIDShort = "35" + //we make this look like a valid IMEI
                Build.BOARD.length() % 10 + Build.BRAND.length() % 10 +
                cpuABILenght % 10 + Build.DEVICE.length() % 10 +
                Build.DISPLAY.length() % 10 + Build.HOST.length() % 10 +
                Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10 +
                Build.MODEL.length() % 10 + Build.PRODUCT.length() % 10 +
                Build.TAGS.length() % 10 + Build.TYPE.length() % 10 +
                Build.USER.length() % 10; //13 digits

        if (m_szImei == null) {
            m_szImei = "";
        }

        if (m_szDevIDShort == null) {
            m_szDevIDShort = "";
        }

        if (m_szAndroidID == null) {
            m_szAndroidID = "";
        }

        String m_szLongID = m_szImei + m_szDevIDShort + m_szAndroidID; //+ m_szWLANMAC + m_szBTMAC;
        // compute md5
        MessageDigest m = null;
        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        m.update(m_szLongID.getBytes(), 0, m_szLongID.length());
        // get md5 bytes
        byte p_md5Data[] = m.digest();
        // create a hex string
        String m_szUniqueID = new String();
        for (int i = 0; i < p_md5Data.length; i++) {
            int b = (0xFF & p_md5Data[i]);
            // if it is a single digit, make sure it have 0 in front (proper padding)
            if (b <= 0xF) m_szUniqueID += "0";
            // add number to string
            m_szUniqueID += Integer.toHexString(b);
        }
        // hex string to uppercase
        m_szUniqueID = m_szUniqueID.toUpperCase();

        return m_szUniqueID;
    }*/
}
