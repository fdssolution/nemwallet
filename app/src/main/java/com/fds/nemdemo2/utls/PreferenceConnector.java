package com.fds.nemdemo2.utls;

import android.content.Context;
import android.content.SharedPreferences;

import com.fds.nemdemo2.BuildConfig;
import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Set;

/**
 * Created by NCPTSCTV on 1/21/2016.
 */
public class PreferenceConnector {
    public static final String PREF_NAME = BuildConfig.APPLICATION_ID;
    //    public static final String PREF_NAME_REMEMBER = "ENUMERATOR_REMEMBER";
    public static final int MODE = Context.MODE_PRIVATE;

//    public static final String name = "name";

    public static void writeBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).commit();
    }

    public static boolean readBoolean(Context context, String key,
                                      boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    public static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).commit();

    }

    public static int readInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    public static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).commit();

    }

    public static String readString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    public static void writeLong(Context context, String key, long value) {
        getEditor(context).putLong(key, value).commit();
    }

    public static long readLong(Context context, String key, long defValue) {
        return getPreferences(context).getLong(key, defValue);
    }

    public static void writeObject(Context context, String key, Object objValue) {
        try {
            Gson gson = new Gson();
            String json = gson.toJson(objValue);
            getEditor(context).putString(key, json).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Object readObject(Context context, String key, Class cls) {
        Gson gson = new Gson();
        String json = getPreferences(context).getString(key, "");
        Object obj = gson.fromJson(json, cls);

        return obj;
    }

    public static ArrayList readArrayListObject(Context context, String key, Type type) {
        Gson gson = new Gson();
        String json = getPreferences(context).getString(key, "");
        ArrayList obj = gson.fromJson(json, type);

        return obj;
    }

    public static void writeArrayString(Context context, String key, String[] arrValue) {
        String str = Utilities.stringJoin(arrValue, ",");
        getEditor(context).putString(key, str).commit();
    }

    public static String[] readArrayString(Context context, String key) {
        String[] stringsArray = new String[0];

        try {
            String str = readString(context, key, "");
            // bo dau "[" va "]" trong chuoi
            str = str.substring(1, str.length() - 1);
            str = str.replace("[", "")
                    .replace("]", "");
            stringsArray = str.split(",");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return stringsArray;
    }

    public static boolean checkExistKey(Context context, String key) {
        return getPreferences(context).contains(key);
    }

    public static boolean deleteKey(Context context, String key) {
        return getEditor(context).remove(key).commit();
    }

    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    public static SharedPreferences.Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

    public static void writeSet(Context context, String key, Set<String> value) {
        getEditor(context).putStringSet(key, value).commit();
    }

    public static Set<String> readSet(Context context, String key, Set<String> defValue) {
        return getPreferences(context).getStringSet(key, defValue);
    }
}
