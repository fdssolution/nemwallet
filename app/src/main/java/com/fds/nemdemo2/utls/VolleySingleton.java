package com.fds.nemdemo2.utls;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.fds.nemdemo2.application.AppController;

/**
 * Created by anh-pc on 10/31/2015.
 */
public class VolleySingleton {
    private static final String TAG = VolleySingleton.class.getSimpleName();
    private static VolleySingleton instance;
    private RequestQueue requestQueue;
    private ImageLoader imageLoader;

    private VolleySingleton(Context context) {
        requestQueue = getRequestQueue();
        imageLoader = new ImageLoader(requestQueue, new LRUBitmapCache(LRUBitmapCache.getCacheSize(context)));
    }

    /**
     * @return the instance of the {@link VolleySingleton}.
     */
    public static synchronized VolleySingleton getInstance(Context context) {
        if (instance == null) {
            instance = new VolleySingleton(context);
        }
        return instance;
    }

    /**
     * @return the instance of the {@link VolleySingleton}.
     */
    public static synchronized VolleySingleton getInstance() {
        if (instance == null) {
            instance = new VolleySingleton(AppController.context);
        }
        return instance;
    }

    /**
     * @return the instance of the {@link RequestQueue}.
     */
    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(AppController.context);
        }
        return requestQueue;
    }

    /**
     * @return the instance of the {@link ImageLoader}.
     */
    public ImageLoader getImageLoader() {
        return imageLoader;
    }

    /**
     * Add Request vào RequestQueue theo tag
     * @param req Request truyền vào
     * @param tag Dùng để gán request theo tag nào
     *
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    /**
     * Add Request vào RequestQueue dùng tag mặc định
     * @param req Request truyền vào
     *
     */
    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    /**
     * Dùng để cancel các request đang chạy theo tag
     * @param tag Tag để cancel các request được gán theo nó
     */
    public void cancelPendingRequests(Object tag) {
        if (requestQueue != null) {
            requestQueue.cancelAll(tag);
        }
    }
}
