package com.fds.nemdemo2.utls;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.fds.nemdemo2.BuildConfig;
import com.fds.nemdemo2.application.Constant;

/**
 * Debug logs. Using handle error and debug code
 *
 * @author PhuongTNM
 * @version 2.1
 */
public class DebugLog {

    public static final String TAG = BuildConfig.APPLICATION_ID;
    private static final int DOT_LEVEL = 4; //3 package name dots + 1

    private static boolean hasDebuggable() {
        return Constant.DEBUGGABLE;
    }

    /**
     * <pre>
     * Show log with Debug able in android
     *
     * @param logType includes Log.DEBUG, INFO, VERBOSE, WARN, ERROR
     * @param tag     name for search
     * @param msg     message when show log
     */
    public static void show(int logType, String tag, String msg) {
        if (!hasDebuggable()) {
            return;
        }
        if (null == tag) {
            tag = TAG;
        }

        switch (logType) {
            case Log.ERROR:
                Log.e(tag, log(msg, DOT_LEVEL));
                break;

            case Log.DEBUG:
                Log.d(tag, log(msg, DOT_LEVEL));
                break;

            case Log.INFO:
                Log.i(tag, log(msg, DOT_LEVEL));
                break;

            case Log.VERBOSE:
                Log.v(tag, log(msg, DOT_LEVEL));
                break;

            case Log.WARN:
                Log.w(tag, log(msg, DOT_LEVEL));
                break;

            default:
                Log.d(tag, log(msg, DOT_LEVEL));
                break;
        }
    }

    /**
     * Show log with log type and message
     *
     * @param logType includes Log.DEBUG, INFO, VERBOSE, WARN, ERROR
     * @param msg     message when show log
     */
    public static void show(int logType, String msg) {
        show(logType, null, msg);
    }

    /**
     * Show log (Log.DEBUG) with tag and message
     *
     * @param tag filter tag
     * @param msg message when show log
     */
    public static void show(String tag, String msg) {
        show(0, tag, msg);
    }

    /**
     * Show log (Log.DEBUG) with message
     *
     * @param msg message when show log
     */
    public static void show(String msg) {
        show(0, null, msg);
    }

    /**
     * General message
     *
     * @param msg
     * @param level
     * @return string with
     * <pre>
     * ClassName.methodName():lineNumber
     * => message more
     * </pre>
     */
    private static String log(String msg, int level) {
        String className = Thread.currentThread().getStackTrace()[level].getClassName();

        String methodName = Thread.currentThread().getStackTrace()[level].getMethodName();
        int lineNumber = Thread.currentThread().getStackTrace()[level].getLineNumber();

        String result = className + "." + methodName + "():" + lineNumber;
        if (null != msg && !"".equals(msg)) {
            result = result + "\n=> " + msg;
        }
        return result;
    }

    /**
     * Show log (Log.DEBUG)
     */
    public static void showError(String msg) {
        show(Log.ERROR, null, msg);
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }
}
