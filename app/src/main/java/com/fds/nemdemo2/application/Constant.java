package com.fds.nemdemo2.application;

import com.fds.nemdemo2.BuildConfig;

public class Constant {
    public static final boolean DEBUGGABLE = BuildConfig.DEBUG;
    public static String SERVER_TEST_NET = "http://188.68.50.161";
    public static String DEFAULT_PORT = "7890";
    public static String API_URL = SERVER_TEST_NET + ":" + DEFAULT_PORT + "/";
}
