package com.fds.nemdemo2.application;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import java.lang.ref.SoftReference;

public class AppController extends Application {
    public static AppController appController;
    public static Context context;
    private static volatile SoftReference<Handler> _mainHandler = new SoftReference<>(null);

    @Override
    public void onCreate() {
        super.onCreate();
        appController = this;
        context = getApplicationContext();
    }

    public static Context getAppContext() {
        return context;
    }

    public static String getResString(@StringRes int resId, @Nullable Object... formatArgs) {
        return formatArgs != null && formatArgs.length > 0
                ? getAppContext().getString(resId, formatArgs)
                : getAppContext().getString(resId);
    }

    /**
     * Returns {@link Handler} for the main thread
     */
    public static synchronized Handler getMainHandler() {
        Handler handler = _mainHandler.get();
        if (handler == null) {
            handler = new Handler(Looper.getMainLooper());
            _mainHandler = new SoftReference<>(handler);
        }
        return handler;
    }

}
