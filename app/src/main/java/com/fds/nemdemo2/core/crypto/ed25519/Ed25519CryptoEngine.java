package com.fds.nemdemo2.core.crypto.ed25519;

import com.fds.nemdemo2.core.crypto.BlockCipher;
import com.fds.nemdemo2.core.crypto.CryptoEngine;
import com.fds.nemdemo2.core.crypto.Curve;
import com.fds.nemdemo2.core.crypto.DsaSigner;
import com.fds.nemdemo2.core.crypto.KeyAnalyzer;
import com.fds.nemdemo2.core.crypto.KeyGenerator;
import com.fds.nemdemo2.core.crypto.KeyPair;


/**
 * Class that wraps the Ed25519 specific implementation.
 */
public class Ed25519CryptoEngine implements CryptoEngine {

    @Override
    public Curve getCurve() {
        return Ed25519Curve.ed25519();
    }

    @Override
    public DsaSigner createDsaSigner(final KeyPair keyPair) {
        return new Ed25519DsaSigner(keyPair);
    }

    @Override
    public KeyGenerator createKeyGenerator() {
        return new Ed25519KeyGenerator();
    }

    @Override
    public BlockCipher createBlockCipher(final KeyPair senderKeyPair, final KeyPair recipientKeyPair) {
        return new Ed25519BlockCipher(senderKeyPair, recipientKeyPair);
    }

    @Override
    public KeyAnalyzer createKeyAnalyzer() {
        return new Ed25519KeyAnalyzer();
    }
}
