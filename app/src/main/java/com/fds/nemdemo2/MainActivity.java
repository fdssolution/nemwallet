package com.fds.nemdemo2;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fds.nemdemo2.nac.activities.NacBaseActivity;
import com.fds.nemdemo2.nac.models.Xems;
import com.fds.nemdemo2.restapi.account.AccountRequestDataAPI2;
import com.fds.nemdemo2.restapi.base.RestAPINetworkBase;
import com.fds.nemdemo2.restapi.models2.AccountRequestDataResult;
import com.fds.nemdemo2.utls.DebugLog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.editText_private_key)
    EditText editTextPrivateKey;
    @BindView(R.id.button_get_acc_info)
    Button buttonGetAccInfo;
    @BindView(R.id.textView2)
    TextView textView2;
    @BindView(R.id.textView3)
    TextView textView3;
    @BindView(R.id.textView4)
    TextView textView4;

    private String privateKey = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        init();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void init() {
        editTextPrivateKey.setText("TC75DUND3DXNSVGBRWCMFZTDD7HWDBG6YIQ4RF3V");
    }

    private void loadAccountInfo() {
        String privateKey = editTextPrivateKey.getText().toString();
        AccountRequestDataAPI2 accountRequestDataAPI = new AccountRequestDataAPI2();
        accountRequestDataAPI.setCompleteResponseListener(new RestAPINetworkBase.OnCompleteResponseListener() {
            @Override
            public void OnCompleteResponse(Object obj) {
                AccountRequestDataResult result = (AccountRequestDataResult) obj;

                Xems xem = Xems.fromMicro(result.getAccount().getBalance().longValue());
                textView2.setText("Address: \n" + result.getAccount().getAddress());
                String balance = getString(R.string.label_xems_balance, xem.toFractionalString());
                textView3.setText(NacBaseActivity.fromHtml(balance));
                textView4.setText("Public Key: \n" + result.getAccount().getPublicKey());

                DebugLog.show(xem.toFractionalString());
                DebugLog.show(xem.getAsMicro() + "");
                DebugLog.show(xem.getIntegerPart() + "");
                DebugLog.show(result.getAccount().getPublicKey() + "");
            }
        });

        accountRequestDataAPI.setErrorResponseListener(new RestAPINetworkBase.OnErrorResponseListener() {
            @Override
            public void OnErrorResponse(String error) {
                DebugLog.showError(error);
            }
        });

        accountRequestDataAPI.requestTheAccountData(privateKey);
    }

    @OnClick(R.id.button_get_acc_info)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_get_acc_info:
                privateKey = editTextPrivateKey.getText().toString();
                if (!privateKey.isEmpty()) {
                    loadAccountInfo();
                } else {
                    editTextPrivateKey.setError("Please input correct Private Key");
                }

                break;
        }
    }
}
