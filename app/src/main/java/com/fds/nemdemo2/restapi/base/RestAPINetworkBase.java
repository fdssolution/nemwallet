package com.fds.nemdemo2.restapi.base;

import com.android.volley.Response;
import com.fds.nemdemo2.application.Constant;
import com.fds.nemdemo2.utls.CustomRequest;
import com.fds.nemdemo2.utls.VolleySingleton;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by TD on 11/3/2015.
 *
 * @author TD
 * Class network base để gọi RestAPI
 */
public class RestAPINetworkBase {
    public static final String RESULT_SUCCESS = "1";
    //    public static final String RESULT_SUCCESS_INACTIVE = "2";

    private int bHttpStatusCode = 0;
    private String bApiUrl = "";

    public OnCompleteResponseListener bOnCompleteResponseListener;
    public OnErrorResponseListener bOnErrorResponseListener;

    public interface OnCompleteResponseListener {
        void OnCompleteResponse(Object obj);
    }

    public interface OnErrorResponseListener {
        void OnErrorResponse(String error);
    }

    public void setCompleteResponseListener(OnCompleteResponseListener mOnCompleteResponseListener) {
        this.bOnCompleteResponseListener = mOnCompleteResponseListener;
    }

    public void setErrorResponseListener(OnErrorResponseListener mOnErrorResponseListener) {
        this.bOnErrorResponseListener = mOnErrorResponseListener;
    }

    public int getbHttpStatusCode() {
        return bHttpStatusCode;
    }

    public String getbApiUrl() {
        bApiUrl = Constant.API_URL;
        return bApiUrl;
    }

    public String getTag() {
        return this.getClass().getSimpleName();
    }

    public String getTag(String s) {
        return this.getClass().getSimpleName() + " - " + s;
    }

    /**
     * Hàm goi REST API
     *
     * @param method          Methoa GET, POST, PUT ....
     * @param action          action cua REST API VD : authen
     * @param hMapParams      HashMap chua cac value de post, put ... len API
     * @param hMapQueryString HashMap chua cac value noi chuoi query string
     * @param doneListener    Instance của {@link Response.Listener< JSONObject >}
     * @param errorListener   Instance của {@link Response.ErrorListener errorListener}
     */
    public void doCallREST(int method, String action, HashMap<String, String> hMapParams, HashMap<String, String> hMapQueryString
            , Response.Listener<JSONObject> doneListener, Response.ErrorListener errorListener) {
        String url;
        int i = 1;

        if (hMapParams == null) {
            hMapParams = new HashMap<>();
        }

        if (hMapQueryString == null) {
            hMapQueryString = new HashMap();
        }

        try {
            url = getbApiUrl() + action + "?";
            if (hMapQueryString != null) {
                for (Map.Entry<String, String> entry : hMapQueryString.entrySet()) {
                    if (i == 1) {
                        url += entry.getKey() + "=" + entry.getValue();
                    } else {
                        url += "&" + entry.getKey() + "=" + entry.getValue();
                    }
                }
            }

            CustomRequest request = new CustomRequest(method, url, hMapParams, bHttpStatusCode, doneListener, errorListener);

            VolleySingleton.getInstance().addToRequestQueue(request);
        } catch (Exception ex) {
            throw new RuntimeException("Call Reset Error", ex);
        }
    }

    /**
     * @param method          Methoa GET, POST, PUT ....
     * @param url             Url api
     * @param action          action cua REST API VD : authen
     * @param hMapParams      HashMap chua cac value de post, put ... len API
     * @param hMapQueryString HashMap chua cac value noi chuoi query string
     * @param doneListener    Instance của {@link Response.Listener< JSONObject >}
     * @param errorListener   Instance của {@link Response.ErrorListener errorListener}
     */
    public void doCallREST(int method, String url, String action, HashMap<String, String> hMapParams, HashMap<String, String> hMapQueryString
            , Response.Listener<JSONObject> doneListener, Response.ErrorListener errorListener) {

        try {
            if (hMapParams == null) {
                hMapParams = new HashMap<>();
            }

            if (hMapQueryString == null) {
                hMapQueryString = new HashMap();
            }

            url += action + "?";

            if (hMapQueryString != null) {
                for (Map.Entry<String, String> entry : hMapQueryString.entrySet()) {
                    url += "&" + entry.getKey() + "=" + entry.getValue();
                }
            }

            CustomRequest request = new CustomRequest(method, url, hMapParams, bHttpStatusCode, doneListener, errorListener);

            VolleySingleton.getInstance().addToRequestQueue(request);
        } catch (Exception ex) {
            throw new RuntimeException("Call Reset Error", ex);
        }
    }

    /**
     * goi spotx
     *
     * @param method
     * @param url
     * @param doneListener
     * @param errorListener
     */
    public void doCallREST(int method, String url, Response.Listener<JSONObject> doneListener,
                           Response.ErrorListener errorListener) {
        try {
            HashMap<String, String> hMapParams = new HashMap<>();
            CustomRequest request = new CustomRequest(method, url, hMapParams, bHttpStatusCode, doneListener, errorListener);
            VolleySingleton.getInstance().addToRequestQueue(request);
        } catch (Exception ex) {
            throw new RuntimeException("Call Reset Error", ex);
        }
    }
}
