package com.fds.nemdemo2.restapi.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Account {
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("harvestedBlocks")
    @Expose
    private String harvestedBlocks;
    @SerializedName("balance")
    @Expose
    private long balance;
    @SerializedName("importance")
    @Expose
    private String importance;
    @SerializedName("vestedBalance")
    @Expose
    private long vestedBalance;
    @SerializedName("publicKey")
    @Expose
    private String publicKey;
    @SerializedName("label")
    @Expose
    private Object label;
    @SerializedName("multisigInfo")
    @Expose
    private MultisigInfo multisigInfo;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHarvestedBlocks() {
        return harvestedBlocks;
    }

    public void setHarvestedBlocks(String harvestedBlocks) {
        this.harvestedBlocks = harvestedBlocks;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public String getImportance() {
        return importance;
    }

    public void setImportance(String importance) {
        this.importance = importance;
    }

    public long getVestedBalance() {
        return vestedBalance;
    }

    public void setVestedBalance(long vestedBalance) {
        this.vestedBalance = vestedBalance;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public Object getLabel() {
        return label;
    }

    public void setLabel(Object label) {
        this.label = label;
    }

    public MultisigInfo getMultisigInfo() {
        return multisigInfo;
    }

    public void setMultisigInfo(MultisigInfo multisigInfo) {
        this.multisigInfo = multisigInfo;
    }
}
