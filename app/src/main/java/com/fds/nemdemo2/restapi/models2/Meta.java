
package com.fds.nemdemo2.restapi.models2;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "cosignatories",
        "cosignatoryOf",
        "status",
        "remoteStatus"
})
public class Meta {

    @JsonProperty("cosignatories")
    private List<Object> cosignatories = null;
    @JsonProperty("cosignatoryOf")
    private List<Object> cosignatoryOf = null;
    @JsonProperty("status")
    private String status;
    @JsonProperty("remoteStatus")
    private String remoteStatus;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("cosignatories")
    public List<Object> getCosignatories() {
        return cosignatories;
    }

    @JsonProperty("cosignatories")
    public void setCosignatories(List<Object> cosignatories) {
        this.cosignatories = cosignatories;
    }

    @JsonProperty("cosignatoryOf")
    public List<Object> getCosignatoryOf() {
        return cosignatoryOf;
    }

    @JsonProperty("cosignatoryOf")
    public void setCosignatoryOf(List<Object> cosignatoryOf) {
        this.cosignatoryOf = cosignatoryOf;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("remoteStatus")
    public String getRemoteStatus() {
        return remoteStatus;
    }

    @JsonProperty("remoteStatus")
    public void setRemoteStatus(String remoteStatus) {
        this.remoteStatus = remoteStatus;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
