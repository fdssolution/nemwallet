package com.fds.nemdemo2.restapi.account;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fds.nemdemo2.restapi.base.RestAPINetworkBase;
import com.fds.nemdemo2.restapi.models2.AccountRequestDataResult;

import org.json.JSONObject;

import java.util.HashMap;

public class AccountRequestDataAPI2 extends RestAPINetworkBase {
    private static final String TAG = AccountRequestDataAPI2.class.getCanonicalName();
    private ObjectMapper objectMapper = new ObjectMapper();

    public void requestTheAccountData(String privateKey) {
        try {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("address", privateKey);

            doCallREST(Request.Method.GET, "account/get", null, hashMap, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        AccountRequestDataResult result = objectMapper.readValue(response.toString(), AccountRequestDataResult.class);
                        bOnCompleteResponseListener.OnCompleteResponse(result);
                    } catch (Exception e) {
                        String err = (e.getMessage() == null) ? "Unknown Error" : e.getMessage();
                        Log.e(TAG, err);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (getbHttpStatusCode() == 401 || getbHttpStatusCode() == 503) {
                        bOnErrorResponseListener.OnErrorResponse(getbHttpStatusCode() + "");
                    } else {
                        bOnErrorResponseListener.OnErrorResponse(error == null ? "Unknown Error" : error.toString());
                    }
                }
            });
        } catch (Exception e) {
            throw new RuntimeException(TAG, e);
        }
    }
}
