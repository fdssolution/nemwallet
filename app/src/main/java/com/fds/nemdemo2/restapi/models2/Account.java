
package com.fds.nemdemo2.restapi.models2;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "address",
        "harvestedBlocks",
        "balance",
        "importance",
        "vestedBalance",
        "publicKey",
        "label",
        "multisigInfo"
})
public class Account {

    @JsonProperty("address")
    private String address;
    @JsonProperty("harvestedBlocks")
    private Integer harvestedBlocks;
    @JsonProperty("balance")
    private Double balance;
    @JsonProperty("importance")
    private Integer importance;
    @JsonProperty("vestedBalance")
    private Double vestedBalance;
    @JsonProperty("publicKey")
    private String publicKey;
    @JsonProperty("label")
    private Object label;
    @JsonProperty("multisigInfo")
    private MultisigInfo multisigInfo;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("harvestedBlocks")
    public Integer getHarvestedBlocks() {
        return harvestedBlocks;
    }

    @JsonProperty("harvestedBlocks")
    public void setHarvestedBlocks(Integer harvestedBlocks) {
        this.harvestedBlocks = harvestedBlocks;
    }

    @JsonProperty("balance")
    public Double getBalance() {
        return balance;
    }

    @JsonProperty("balance")
    public void setBalance(Double balance) {
        this.balance = balance;
    }

    @JsonProperty("importance")
    public Integer getImportance() {
        return importance;
    }

    @JsonProperty("importance")
    public void setImportance(Integer importance) {
        this.importance = importance;
    }

    @JsonProperty("vestedBalance")
    public Double getVestedBalance() {
        return vestedBalance;
    }

    @JsonProperty("vestedBalance")
    public void setVestedBalance(Double vestedBalance) {
        this.vestedBalance = vestedBalance;
    }

    @JsonProperty("publicKey")
    public String getPublicKey() {
        return publicKey;
    }

    @JsonProperty("publicKey")
    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    @JsonProperty("label")
    public Object getLabel() {
        return label;
    }

    @JsonProperty("label")
    public void setLabel(Object label) {
        this.label = label;
    }

    @JsonProperty("multisigInfo")
    public MultisigInfo getMultisigInfo() {
        return multisigInfo;
    }

    @JsonProperty("multisigInfo")
    public void setMultisigInfo(MultisigInfo multisigInfo) {
        this.multisigInfo = multisigInfo;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }


}
