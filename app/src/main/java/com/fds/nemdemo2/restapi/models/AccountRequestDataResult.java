package com.fds.nemdemo2.restapi.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AccountRequestDataResult extends GeneralResult {
    @Expose
    private Meta meta;
    @SerializedName("account")
    @Expose
    private Account account;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

}
