package com.fds.nemdemo2.restapi.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Meta {
    @SerializedName("cosignatories")
    @Expose
    private List<Object> cosignatories = null;
    @SerializedName("cosignatoryOf")
    @Expose
    private List<Object> cosignatoryOf = null;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("remoteStatus")
    @Expose
    private String remoteStatus;

    public List<Object> getCosignatories() {
        return cosignatories;
    }

    public void setCosignatories(List<Object> cosignatories) {
        this.cosignatories = cosignatories;
    }

    public List<Object> getCosignatoryOf() {
        return cosignatoryOf;
    }

    public void setCosignatoryOf(List<Object> cosignatoryOf) {
        this.cosignatoryOf = cosignatoryOf;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemoteStatus() {
        return remoteStatus;
    }

    public void setRemoteStatus(String remoteStatus) {
        this.remoteStatus = remoteStatus;
    }
}
